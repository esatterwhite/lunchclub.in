/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true*/
'use strict';
/**
 * club.js
 * @module club.js
 * @author 
 * @since 0.0.1
 * @requires zim
 * @requires moduleB
 * @requires moduleC
 */

var connection = require( 'zim/lib/db' ).connection
  , r          = connection.default.r
  , fields     = connection.default.type
  , skoodge    = require('skoodge')
  , User       = skoodge.getUserModel()
  , Resturaunt = require('./resturaunt')
  , Club = require('./club')
  , Event
  ; 

/**
 * Description
 * @constructor
 * @alias module:club.js
 * @param {TYPE} [param]
 * @param {TYPE} [?param.val=1j]
 * @example var x = new require('club.js');
 */
Event = connection.default.createModel('lunchclub_core_event',{
    name           : fields.string().required()
    ,resturaunt_id : fields.string()
    ,club_id       : fields.string()
    ,user_id       : fields.string()
    ,starts_at     : fields.date().required()
    ,created_at    : fields.date().default( ()=> { return new Date(); })
},{
	pk:'club_event_id'
});

Event.define('upvote', function( user_id ){
  return new Promise(function( resolve, reject ){

    User
      .get(user_id)
      .then( 
          function( user ){
            that.update(( event )=>{
              return {votes: event('votes').setInsert( user )}
            })
            .run()
            .then(( evt )=>{
              resolve( evt );
            })
          }
        , function( err ){}
      )
  });
});

Event.belongsTo( Resturaunt, 'resturant', 'resturaunt_id', Resturaunt._pk );
Event.belongsTo( User, 'creator', 'user_id', User._pk );
Event.belongsTo( Club, 'club', 'club_id',  Club._pk );

module.exports = Event;
