/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true*/
'use strict';
/**
 * user
 * @module lunch-club/resources/v1/user
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires tastypie
 * @requires skoodge/models/user
 */

var path = require('path') 
  , tastypie = require( 'tastypie' )
  , Resource = require('@tastypie/rethink')
  , skoodge  = require('skoodge')
  , JSONSchema = require('@tastypie/jsonschema')
  , User = skoodge.getUserModel()
  , UserResource
  ;

/**
 * Description
 * @constructor
 * @alias module:user
 * @param {TYPE} [param]
 * @param {TYPE} [?param.val=1j]
 * @example var x = new require('user');
 */

UserResource = Resource.extend({
	mixin:[JSONSchema]
	,options:{
		queryset:User.secure()
		,name:'user'
		,labelField:'last_name'
		,pk:'auth_user_id'
		,filtering:{
			username:['startswith', 'contains', 'endswith']
		}
	}
	,fields:{
		 username    : { type:'char', nullable: false, blank:false , help:"user name on registered account"}
		,first_name  : {type:'char', nullable: false, blank:false, attribute:'name.first'}
		,last_name   : {type:'char', nullable: false, blank:false, attribute:'name.last'}
		,email       : {type:'char', nullable: false, blank:false, help:'unique email address on the account'}
		,loginDate   : {type:'datetime', readonly:true}
		,active      : {type:'bool', readonly:true, default: true}
		,superuser   : {type:'bool', readonly:true, default: false}
		,roles       : {type:'array', to: path.join(__dirname, 'role'), nullable: true }
		,permissions : {type:'object', readonly: true}
	}
	
	, get_object: function( bundle, callback ){
		this.options
			.queryset
			._model
			.get( bundle.req.params.pk )
			.withPermissions()
			.secure()
			.run(callback);

		return this;
	}

	, create_object: function(bundle, callback ){
		var format = this.format( bundle, this.options.serializer.types )
		  , that = this
		  ;

		this.deserialize( bundle.data, format, function( err, data ){
			data = data || {}
			
			data.confirm = data.password = undefined;
			bundle.object = that.options.queryset._model( data );
			that.full_hydrate( bundle, function(err,bndl){
				try{
					bundle.object.validate();
				} catch( err ){
					err.code = err.name == 'ValidationError' ? 400 : 500
					return callback( err, null )
				}
				bundle.object.saveAll(function(e, obj){
					bundle.object = obj;
					bundle.data = obj;
					skoodge.emit('account_created', obj, `api/v1//${obj.id}/account/confirm`)
					return callback(e, bndl)
				})
			})
		})
	}
})

module.exports = UserResource
