/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true*/
'use strict';
/**
 * club.js
 * @module club.js
 * @author Eric Satterwhire
 * @since 0.0.1
 * @requires zim
 * @requires moduleB
 * @requires moduleC
 */

var connection = require( 'zim/lib/db' ).connection
  , util       = require('util')
  , r          = connection.default.r
  , fields     = connection.default.type
  , skoodge    = require('skoodge')
  , User       = skoodge.getUserModel()
  , auth       = require('skoodge')
  , Club
  ;

/**
 * Description
 * @constructor
 * @alias module:club.js
 * @param {TYPE} [param]
 * @param {TYPE} [?param.val=1j]
 * @example var x = new require('club.js');
 */
Club = connection.default.createModel('lunchclub_core_club',{
	name: fields.string().required(),
	description:fields.string().required(),
	owner_id: fields.string(),
    active: fields.boolean()
});

Club.define('transfer', function( from, to ){
	let owner = this.owner;
	let promises = [];

	if( owner[ User._pk ] != from[ User._pk] ){
		return Promise.reject(new Error('Invalid owner'));
	}

	if( this.isSaved() && owner && user instanceof User && user.isSaved() ){
		promises.push( this.removeRelation('owner') );
		promises.push( this.addRelation('owner', user) );
	}

	return Promise.all( promises );
});

Club.define('isOwner', function( user ){

	if( String.isString( user ) ){
		user = { [User._pk]: user }
	}

	return ( user[User._pk] == this.owner_id);
});

Club.define('ban', function( user ){
	if(String.isString( user )){
		user = new User({[User._pk]: user });
	}

	return this.removeRelation('members', user)
});


Club.hasAndBelongsToMany( User, 'members', 'id','auth_user_id' );
Club.hasOne(User, 'owner', 'id', 'auth_user_id');
User.hasAndBelongsToMany( Club, 'clubs', 'auth_user_id','id' );


module.exports = Club;
