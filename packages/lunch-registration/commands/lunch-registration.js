/*jshint laxcomma: true, smarttabs: true, node: true, esnext: true*/
'use strict';
/**
 * Default command
 * @module lunch-registration/commands/default
 * @author 
 * @since 1.0.0
 * @requires seeli
 */

var cli = require( 'seeli' );

/**
 * @alias module:lunch-registration/commands/lunch-registration
 * @param {TYPE} param
 * @example var x = new _commands.js.THING();
 */
module.exports = new cli.Command({
    description:"Default command for default package"
    ,usage:[
        cli.bold('Usage: ') + 'cli default --help'
      , cli.bold('Usage: ') + 'cli default --no-color'
      , cli.bold('Usage: ') + 'cli default -i'
    ]

    ,flags:{
        'enabled':{
            type: Boolean
            ,description:"Enable the default"
            ,default:true
            ,required:false
        }
    }
    /**
     * This does something
     * @param {String|null} directive a directive passed in from the cli
     * @param {Object} data the options collected from the cli input
     * @param {Function} done the callback function that must be called when this command has finished
     * @returns something
     **/
    ,run: function( cmd, data, done){

        done(/* error */ null, /* output */ 'success')
    }
});
