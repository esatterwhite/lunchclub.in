/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true*/
'use strict';
/**
 * cuisine.js
 * @module cuisine.js
 * @author 
 * @since 0.0.1
 * @requires tastypie
 * @requires Club
 * @requires moduleC
 */

var tastypie     = require( 'tastypie' )
  , Resource     = require('@tastypie/rethink')
  , JSONSchema   = require('@tastypie/jsonschema')
  , Cuisine        = require( '../../models/cuisine' )
  , UserResource = require("./user")
  , CuisineResource
  ;

/**
 * Description
 * @constructor
 * @alias module:cuisine.js
 * @param {TYPE} [param]
 * @param {TYPE} [?param.val=1j]
 * @example var x = new require('cuisine.js');
 */

CuisineResource = Resource.extend({
	mixin:[JSONSchema]
	,options:{
		queryset:Cuisine.getJoin().filter({}),
		name:'cuisine',
		labelField:'name'
	}
	,fields:{
		name        : { type:'char' }
	  , description : { type:'char' }
	  , name        : { type:'char', enum:['cheap','moderate','pricey'], default: 'moderate' }
	}
})

module.exports = CuisineResource
