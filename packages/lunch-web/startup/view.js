'use strict';
const templates = require('zim/lib/loading/templates');

module.exports = function( server ){
	const ui = server.select('ui');
	ui.register( [ require('inert') ], (err)=>{
		ui.decorate('reply', 'template', function( template, context, options ){
			let response = this.request.generateResponse(templates.render( template, context )) 
			response.encoding('utf8');
			reponse.contentType('text/html');
			this.response( response );
		});
	});
}
