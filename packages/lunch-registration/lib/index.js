/*jshint node:true, laxcomma: true, smarttabs: true, esnext: true*/
'use strict';
/**
 * THIS MODULE DOES THINGS
 * @module MODULE NAME
 * @author YOUR NAME GOES HERE
 * @since 0.1.0
 */

exports.moduleA = require( 'moduleA' )  // this is moduleA
exports.moduleB = require( 'moduleB' )  // this is moduleB
exports.moduleC = require( 'moduleC' )  // this is moduleC
