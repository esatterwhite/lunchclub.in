'use strict';
var tastypie = require('tastypie')
  , Loader = require('gaz/fs/loader')
  , Api = tastypie.Api
  , v1

var loader = new Loader({
  filepattern:/.js$/,
  recurse: false,
  searchpath:'resources/v1'
});

v1 = new Api( 'api/v1',{select:'api'} );

loader
	.load()
	.flat()
	.forEach( function( resource ){
		v1.use( new resource )
	})


module.exports = v1;
