/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true*/
'use strict';
/**
 * Vote
 * @module Vote
 * @author Eric Satterwhite 
 * @since 0.0.1
 * @requires zim
 */

var connection = require( 'zim/lib/db' ).connection
  , r          = connection.default.r
  , fields     = connection.default.type
  , User       = require('skoodge').getUserModel()
  , Event      = require('./event')
  , Vote
  ;

/**
 * Description
 * @constructor
 * @alias module:Vote
 * @param {TYPE} [param]
 * @param {TYPE} [?param.val=1j]
 * @example var x = new require('Vote');
 */
Vote = connection.default.createModel('lunchclub_core_vote',{
	created_at: fields.date().default(function(){ return new Date() }),
	value     : fields.number().integer().min(-1).max(1),
	user_id   : fields.string(),
	event_id  : fields.string()
});

Vote.defineStatic('for', function(event, user){
	return Vote.branch(
		Vote
			.filter({user_id: user[User.pk], event_id:event[Event._pk]})
			.count()
			.gte(1),

			new Vote({
				user:user,
				event:event,
				created_at: r.now(),
				value: 1
			}).saveAll({user:true, event:true}),

			r.expr(false)
	)
});

Vote.hasOne(User,'user', 'id', 'user_id');
Vote.hasOne(Event, 'event', 'club_event_id','event_id' );

module.exports = Vote;