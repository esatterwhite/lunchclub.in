/*jshint laxcomma: true, smarttabs: true, node: true, esnext: true*/
'use strict';
/**
 * Configuration options for lunch-registration
 * @module lunch-registration/conf
 * @author 
 * @since 0.1.0
 */
exports._FOO = 1
exports._THINGS = {
	BAR:{
		BAZ:2
	}
}
