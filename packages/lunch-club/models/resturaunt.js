/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true*/
'use strict';
/**
 * club.js
 * @module lunch-club/models/resturant
 * @author Eric Satterwhite 
 * @since 0.0.1
 * @requires zim
 * @requires skoodge
 */

var connection = require( 'zim/lib/db' ).connection
  , r          = connection.default.r
  , fields     = connection.default.type
  , skoodge    = require('skoodge')
  , Resturaunt
  ;

/**
 * Description
 * @constructor
 * @alias module:club.js
 * @param {TYPE} [param]
 * @param {TYPE} [?param.val=1j]
 * @example var x = new require('club.js');
 */
Resturaunt = connection.default.createModel('lunchclub_core_resturaunt',{
	name: fields.string().required(),
    active: fields.boolean().default(true),
    description:fields.string().required(),
    price_range:fields.string().required().enum(['cheap', 'moderate', 'pricey']),
    address:{
    	city: fields.string().required(),
    	state:fields.string().length(2),
    	zip: fields.number().required(),
    	street: fields.string().required()
    }
});

Resturaunt.ensureIndex('name');
module.exports = Resturaunt;
