#!/usr/bin/env node
/*jshint laxcomma: true, smarttabs: true, esnext: true, node: true, mocha: true*/
var path = require('path')
  , seeli = require('seeli')
  , child_process = require('child_process')
  , Start
  ;

Start = new seeli.Command({
    name:'start'
    ,description:"Start the web server"
    ,usage:[]
    ,flags:{}
    ,run: function( cmd, data, done ){
		var clone = require('gaz/lang').clone         // mout clone function
		  , args  = clone( process.argv )              // process arguments flags
		  , env   = clone( process.env )               // current host environment variables
		  , npath = ( env.NODE_PATH || '' )            // existing node path
		  				.split( path.delimiter ) 
		  , root                                       // root director of this project 
		  , pkgdir                                     // director for packages
		  , server                                     // server child process
		  ;

		args.splice(0,3);
		args.unshift( path.join( __dirname,'..', 'index.js' ) );
		args.unshift('--harmony');
	
		root          = path.resolve( __dirname, '..', '..', '..');
		pkgdir        = path.join( root, 'packages' );
		npath.push( pkgdir );
		env.NODE_PATH = npath.join( path.delimiter );

		server = child_process.spawn( process.execPath, args, {
			cwd: root
			,env:env
		});
		
		server.stdout.pipe( process.stdout );
		server.stderr.pipe( process.stderr );
    }
});

module.exports = Start;
