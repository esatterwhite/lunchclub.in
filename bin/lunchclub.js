#!/bin/sh
":" //# comment; exec /usr/bin/env node --harmony "$0" "$@"
/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Command line Interface for lunchclub. This module is here to load other management
 * commands. And that is about it.
 * @module lunchclub/bin/lunchclub
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires seeli
 * @requires path
 * @requires fs
 * @requires debug
 */

 var cli = require( 'seeli' )
   , fs            = require('fs')   // fs module
   , path          = require('path') // path module
   , conf          = require('keef') // keef config module
   , Class         = require('gaz/class')
   , Loader        = require('gaz/fs').Loader
   , debug         = require('debug')( 'lunchclub:bin' )
   , jsregex       = /\.js$/
   , Commands
   , loader
   ;

var known_apps = {
  'skoodge':'auth'
}
Commands = new Class({
  inherits:Loader
  ,options:{
    searchpath:'commands'
    , filepattern:/\.js$/
    , recurse:true
  }
})

loader = new Commands();
let groups = loader.find()
for ( let grouping in  groups ){
  console.log( cli.red(known_apps[grouping] || grouping)  );
  console.log( groups[grouping] )
}
loader
  .load()
  .flat()
  .forEach(function( cmd ){
    cli.use( cmd.options.name, cmd);
  })

cli.set('color', 'green');
cli.run();
