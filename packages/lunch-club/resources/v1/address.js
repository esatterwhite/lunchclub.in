

/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true*/
'use strict';
/**
 * address.js
 * @module address.js
 * @author Eric Satterwhite 
 * @since 1.0.0
 * @requires tastypie
 * @requires tastypie-rethink
 * @requires tastypie-jsonschema
 */

var tastypie     = require( 'tastypie' )
  , Resource     = tastypie.Resource 
  , JSONSchema   = require('@tastypie/jsonschema')
  , AddressResource
  ;


AddressResource = Resource.extend({
	mixin: [JSONSchema]
	,options:{
		name:'address'
		,includeUri: false
	}
	,fields:{
		 city    : { type:'char'}
		,street  : { type: 'char' }
		,state : { type:'char'}
		,zip     : { type:'char' }
	}

	,constructor:function( options ){
		this.parent('constructor', options );
	}
});

module.exports = AddressResource;
