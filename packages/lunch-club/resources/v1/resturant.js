
/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true*/
'use strict';
/**
 * club.js
 * @module club.js
 * @author Eric Satterwhite 
 * @since 1.0.0
 * @requires tastypie
 * @requires tastypie-rethink
 * @requires tastypie-jsonschema
 */

var path = require('path')
  , tastypie     = require( 'tastypie' )
  , Resource     = require('@tastypie/rethink')
  , JSONSchema   = require('@tastypie/jsonschema')
  , Resturant    = require( '../../models/resturaunt' )
  , ResturantResource
  ;


ResturantResource = Resource.extend({
	mixin: [JSONSchema]
	,options:{
		name:'resturaunt'
		,queryset:Resturant.filter({})
	}
	,fields:{
		 name         : { type:'char'}
		,active      : { type: 'bool' }
		,description : { type:'char'}
		,price_range : { type:'char', enum:['cheap','moderate','pricey'] }
		,address     : { type:'document', to: path.join(__dirname,'address') }
	}
	,constructor:function( options ){
		this.parent('constructor', options );
	}
});

module.exports = ResturantResource;
