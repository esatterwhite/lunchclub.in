/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true*/
'use strict';
/**
 * cuisine
 * @module cuisine
 * @author Eric Satterwhite 
 * @since 0.0.1
 * @requires zim
 */

var connection = require( 'zim/lib/db' ).connection
  , r          = connection.default.r
  , fields     = connection.default.type
  , Cuisine
  ;

/**
 * Description
 * @constructor
 * @alias module:cuisine
 * @param {TYPE} [param]
 * @param {TYPE} [?param.val=1j]
 * @example var x = new require('cuisine');
 */
Cuisine = connection.default.createModel('lunchclub_core_cuisine',{
	  name: fields.string().required(),
    description:fields.string().required(),
    price_range:fields.string().required().enum(['cheap', 'moderate', 'pricey']),
});

Cuisine.ensureIndex('name');
module.exports = Cuisine;
