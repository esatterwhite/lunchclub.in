/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true, esnext: true*/
'use strict';
/**
 * add Good process monitoring to the server with current logger instance
 * @module lunch-web/startup/logger
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires good
 * @requires good-winston
 * @requires @fulfill/cage/logger
 */

var logger = require('bitters')
  , conf = require('keef')
  , Good        = require("good")
  , develop
  ;


Good.options = {
    ops: {
        interval: 1000 * 5
    },
    reporters: {
        winston: [{
            module: 'good-winston'
            ,args:[logger,{
              ops_level:'debug',
              request_level:'http',
              response_level:'http'

            }]
        }]
    }
};
module.exports = Good;
