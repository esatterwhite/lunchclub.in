/*jshint laxcomma: true, smarttabs: true, node:true, esnext: true*/
'use strict';
/**
 * event.js
 * @module event.js
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires tastypie
 * @requires @tastypie/rethink
 * @requires @tastypie/jsonschema
 * @requires lunch-club/models/event
 */

var path         = require('path')
  , async        = require('async')
  , joi          = require('joi')
  , co           = require('co')
  , tastypie     = require( 'tastypie' )
  , Resource     = require('@tastypie/rethink')
  , JSONSchema   = require('@tastypie/jsonschema')
  , interpolate  = require('gaz/string').interpolate
  , Event        = require( '../../models/event' )
  , EventResource
  ;

/**
 * Description
 * @constructor
 * @alias module:event.js
 * @param {TYPE} [param]
 * @param {TYPE} [?param.val=1j]
 * @example var x = new require('event.js');
 */

EventResource = Resource.extend({
	mixin:[JSONSchema]
	,options:{
		queryset:Event.getJoin({resturant:true, club: true, creator: true, votes: true}).filter({}),
		name:'event',
		labelField:'name',
		pk: 'club_event_id',
		config:{
			validate:{
				payload: joi.object().keys({
					starts:joi.date().optional().min('now')
				}).unknown()
			}
		},
		allow:{
			votefeed:{ 
				get:true
				, post:false
				, put: false
				, patch: false
				, delete: false
				, options: false
			}
		}
	}
	,fields:{
		name      : { type:'char' }
      , resturant : { type: 'hasone', to: path.join(__dirname, 'resturant' ) }
	  , creator   : { type: 'hasone', to: path.join(__dirname, 'user' ) }
	  , club      : { type: 'hasone', to: path.join(__dirname, 'club' ) }
      , votes     : { type: 'hasmany', to: path.join(__dirname, 'vote'), readonly: true}
      , starts      : {type:'datetime', nullable:false, attribute:'starts_at'}
	}

	,prepend_urls: function( ){
		return [{
			name:'votefeed'
		  , path: interpolate( tastypie.urljoin( '{{apiname}}','{{name}}', '{pk}', 'votes' ) , this.options ).replace(/\/\//g, '/')
		  , handler: this.dispatch_votefeed.bind( this )
	  	  , config:{ tags:['api'] }
		},{
			name:'upvote'
		  , path: interpolate( tastypie.urljoin( '{{apiname}}','{{name}}', '{pk}', 'upvote') , this.options ).replace(/\/\//g, '/')
		  , handler: this.dispatch_upvote.bind( this )
		  , config:{ 
		  	tags:['api']
		  	,validate:{
		  		payload:joi.object().keys({
		  			creator:joi.string.required()
		  		}).unknown()
		  	}
		  }
		}]
	}

	, dispatch_votefeed: function( request, reply ){
		return this.dispatch('votefeed', this.bundle( request, reply ));
	}

	, dispatch_upvote: function( request, reply ){
		return this.dispatch( 'upvote', this.bundle( request, reply ));
	}

	, get_votefeed: function( bundle ){
		this.parent('bundle', function(err, obj){
			if(!obj){
				bundle.data = {
					message:`No matching event: ${bundle.req.path}`
					,status:404
					,error:'Not Found'
				};
				return this.respond( bundle, tastypie.http.notFound );
			}

			bundle.data = [];
			this.respond( bundle, tastypie.http.accepted );
		});
	}

	, patch_upvote: function( bundle ){
		let format   = this.format( bundle, this.options.serializer.types );
		let event_id = bundle.req.params.pk;
		let user_id  = bundle.req.params.pk

		async.waterfall([
			this.deserialize.bind( this, bundle.req.payload, format )
			this.get_object.bind( this, bundle )
			this.full_hydrate.bind( this, bundle )
		], function( err, results ){
			// upv
			bndl.object
				.upvote( user_id )
				.
		})

		this.deserialize( , function( err, data ){
			if( err ){
				err.res = bundle.res;
				err.req = bundle.req;
				return this.emit('error', err);
			}
			bundle.data = data;
			
			this.get_object( bundel, (get_err, instance)=>{
				bundle.object          = instance
				bundle.object.modified = false;
				bundle.object.setSaved();

				this.full_hydrate( bundle, ( err, bndl )=>{
				});

			})
		}.bind( this ));

	}
})

module.exports = EventResource
