/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true*/
'use strict';
/**
 * vote.js
 * @module vote.js
 * @author Eric Satterwhite 
 * @since 1.0.0
 * @requires tastypie
 * @requires tastypie-rethink
 * @requires tastypie-jsonschema
 */

var path = require('path')
  , tastypie     = require( 'tastypie' )
  , Resource     = tastypie.Resource 
  , JSONSchema   = require('@tastypie/jsonschema')
  , Vote    = require( '../../models/vote' )
  , VoteResource
  ;


VoteResource = Resource.extend({
	mixin: [JSONSchema]
	,options:{
		name:'vote'
	}
	,fields: {
		 created    : { type:'datetime', attr: 'created_at'}
		,street     : { type: 'char' }
		,country    : { type:'char'}
		,creator    : { type:'hasone', to: path.join(__dirname, 'user'), attribute:"user_id"}
	}
	,constructor:function( options ){
		this.parent('constructor', options );
	}
});

module.exports = VoteResource;
