/**
 * Event handers for auth related things
 * @module lunch-club/handlers/auth
 * @author Eric Satterwhite
 **/

var skoodge = require('skoodge');
  , mail    = require('zim/mail')


exports.post = {
	account_created: function( user, uri ){
		mail.send({
			to:user.email,
			from:'lunch@club.in
			template:'
		});
	}
};

skoodge.on( 'account_created', exports.post.account_created );
