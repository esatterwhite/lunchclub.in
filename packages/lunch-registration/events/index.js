/*jshint laxcomma: true, smarttabs: true. node: true, esnext: true*/
'use strict';
/**
 * Primary events module for lunch-registration
 * @module lunch-registration/events
 * @author 
 * @since 0.1.0
 * @requires events
 */

var events = require( 'events' );

module.exports = new events.EventEmitter();
module.exports.setMaxListeners( 50 );
