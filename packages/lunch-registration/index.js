/*jshint laxcomma: true, smarttabs: true, node: true, esnext: true*/
'use strict';
/**
 * Manages Account registraion and recovery
 * @module lunch-registration
 * @author 
 * @since 0.1.0
 * @requires lunch-registration/events 
 * @requires lunch-registration/commands 
 * @requires lunch-registration/models 
 * @requires lunch-registration/lib 
 */


module.exports = require('./events');
	
	Object.assign( module.exports, require('./lib'))
	




// models
module.exports.models = require('./models')

