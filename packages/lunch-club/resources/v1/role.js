/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true*/
'use strict';
/**
 * user
 * @module lunch-club/resources/v1/user
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires tastypie
 * @requires skoodge/models/role
 */

var path = require('path') 
  , tastypie = require( 'tastypie' )
  , Resource = require('@tastypie/rethink')
  , Role  = require('skoodge/models/role')
  , JSONSchema = require('@tastypie/jsonschema')
  , RoleResource
  ;

/**
 * Description
 * @constructor
 * @alias module:user
 * @param {TYPE} [param]
 * @param {TYPE} [?param.val=1j]
 * @example var x = new require('user');
 */
RoleResource = Resource.extend({
	options: {
		name:'role'
	  , pk:'name'
	  , queryset: Role.filter({})
	}

	, fields:{
		permissions: {type: 'object'}
	  , name: { type:'char' }
	}

	,constructor: function( options ){
		this.parent( 'constructor', options );
	}

});

module.exports = RoleResource;

