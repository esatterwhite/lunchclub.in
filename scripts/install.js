#!/usr/bin/env node
/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * configures environment suitable for testing and executes test
 * looks in for test directory in project root and of the packages found in the packages directory
 * @module scripts/test
 * @author Eric Satterwhite
 * @since 1.9.0
 * @requires debug
 * @requires child_process
 * @requires path
 */

 var debug         = console.log                        // the debug module isn't installed at this point. so console.log...
   , child_process = require( 'child_process' )         // node child process module to spawn npm processes
   , path          = require( 'path' )                  // node path module to resolve paths
   , fs            = require( 'fs' )                    // node fs module to check for fields
   , os            = require( 'os' )                    // node os module to check for fields
   , util          = require('util')                    // util module for string formatting
   , npm           = require('npm')                     // npm module
   , semver        = require('npm/node_modules/semver') // npm's current version of semver
   , packages      = []                                 // final list of packages to be installed
   , latest        = {}                                 // cache to weed out dupes at the latest version
   , projectRoot                                        // root directory of this project
   , packagePath                                        // path to the directory where packages are kept
   ;


projectRoot = path.resolve( path.join( ( __dirname ||'.' ), '..' ) );
packagePath = path.resolve( projectRoot, 'packages' );


// attempts to resolve the highest version number 
// available from all of the packages we find
function resolveDeps( dependancies ){
	dependancies = dependancies || {};
	var ret = [] // return value. list of packages to install ( <pkg>@<version> )
	  , current
	  , previous
	  , prefix = /\^|\~/
	; 

	for( var dep in dependancies ){
		if( latest.hasOwnProperty( dep ) ){
			latest[dep] = semver.gt( dependancies[ dep ].replace( prefix, '' ) , latest[ dep ].replace( prefix, '' ) ) ? dependancies[ dep ] : latest[ dep ]
		} else {
			latest[ dep ] = dependancies[ dep ]
		}
	}

	return ret;
}

// find all of the package files
fs.readdirSync( packagePath ).forEach( function( module ){
	var packagefile = path.join( packagePath, module, 'package.json' )

	debug('reading %s', packagefile )

	// if there is a package.json file
	// require it, read the dependencies object and generate a deps array
	if( fs.existsSync( packagefile ) ){
		resolveDeps( require(packagefile).dependencies ) 
	}
});

// flatten object into an array npm will understand
packages = Object
			.keys( latest )
			.sort((a,b)=>{
				return a.split('@')[0].toLowerCase() < b.split('@')[0].toLowerCase() ? -1 : 1
			})
			.map( function( key ){
				return util.format( '%s@%s', key, latest[ key ] );
			})

debug(`installing ${os.EOL}`, packages.join(os.EOL) )


// use npm to install all the the deps into the primary project dir
npm.load(function(){
	npm.prefix = projectRoot;
	npm.commands.install(packages, function(err, packagelist, tree, pretty){
		console.log( pretty )
	})
})
