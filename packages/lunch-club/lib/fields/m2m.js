/*jshint laxcomma: true, smarttabs: true, node: true, esnext: true*/
'use strict';
/**
 * many to many field for rethink resources
 * @module lunch-club/lib/fields/m2m.js
 * @author Eric satterwhite
 * @since 0.0.1
 * @requires gaz/class
 */

var tastypie = require('tastypie')
  , async = require('async')
  , get = require('gaz/object').get
  , RelatedField = require('tastypie/lib/fields/related')
  , Class = require('gaz/class')
  , debug = require('debug')('lunchclub:fields:m2m')
  , M2M
  ;

const STRING = 'string';
const FUNCTION = 'function';

/**
 * Description
 * @class module:m2m.js.Thing
 * @param {TYPE} param
 * @example var x = new m2m.js.THING();
 */

M2M = new Class({
	inherits:RelatedField
	,is_related: true
	,options:{
		to:null,
		minimal: false,
		full: false
	}
	,constructor: function( to, options ){
		// fields can be defined as an options object
		// check for it
		if( !options ){
		    options = to;
		    to = options.to;
		}
		this.parent('constructor', to, options );
		this.options.to = null;
	}
	,dehydrate: function dehydrate( obj, cb ){
		var that = this
		    , attribute = this.options.attribute
		    , name = this.options.name
		    , current
		    ;

		 if( !this.instance.options.apiname ){
		     debug('setting related apiname - %s', this.resource.options.apiname );
		     this.instance.setOptions({
		         apiname: this.resource.options.apiname
		     });
		 }

		 if( typeof attribute === STRING ){
		     current = get( obj, attribute );
		     if( current == null ){
		         if( this.options.default ){
		             current = this.options.default; 
		         } else if( this.options.nullable ){
		             current = null;
		         }
		     }
		     current = typeof current === FUNCTION ? current() : current;
		 } else if( typeof attribute === FUNCTION ){
		     current = attribute( name );
		 }

		 async.map( 
		     current
		     , function( obj, callback ){
		         if( that.options.full ){
		             debug("calling full_dehydrate");
		             that.instance.full_dehydrate( 
		                 obj[attribute] ? obj[attribute] : obj 
		                 , null
		                 , callback 
		             );
		         } else if( that.options.minimal ){
		             that.to_minimal( obj[attribute] ? obj[attribute] : obj , callback );
		         } else {
		             callback( null, obj && that.instance.to_uri( obj )  );
		         }
		     }
		     , cb
		 );
	}
});

Object.defineProperty(tastypie.fields, 'm2m',{
	get: function(){
		return M2M;
	}
});
