/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * simple clean script to remove node_modules except the npm module.
 * @module module:scripts/clean
 * @author Eric Satterwhite
 * @since 1.9.0
 * @requires fs
 * @requires path
 * @requires module:scripts/rimraf
 */

var fs   = require( 'fs' ) // node fs module
  , path = require( 'path' ) // node path module
  , rm   = require('./rimraf') // copy of npm's rimraf script
  , projectRoot
  , directories
  ;

projectRoot = path.resolve( path.join( ( __dirname ||'.' ), '..', 'node_modules' ) );
directories = fs
	.readdirSync(projectRoot);

directories.push( path.resolve( __dirname || '.', '..', 'documentation') );

directories
	.forEach( function( dir ){
		var directory = path.resolve( projectRoot, dir )
		if(dir !== 'npm'){
			rm( directory, function(e){
				if( e ){
					console.log( e )
				}
			} )
		}

	})
