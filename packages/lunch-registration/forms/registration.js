'use strict';
let forms = require('forms')
  , fields = forms.fields
  , validators = forms.validators
 
const reg_form = forms.create({
    username: fields.string({ required: true }),
    password: fields.password({ required: validators.required('you must enter a password') }),
    confirm:  fields.password({
        required: validators.required('you must confirm your password'),
        validators: [validators.matchField('password')]
    }),
    email: fields.email({
    	required:validators.required('you must enter an email')
    })
},{
	validatePastFirstError: true
});

module.exports = reg_form
