/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true*/
'use strict';
/**
 * club.js
 * @module club.js
 * @author 
 * @since 0.0.1
 * @requires tastypie
 * @requires Club
 * @requires moduleC
 */

var tastypie     = require( 'tastypie' )
  , Resource     = require('@tastypie/rethink')
  , JSONSchema   = require('@tastypie/jsonschema')
  , Club         = require( '../../models/club' )
  , UserResource = require("./user")
  , ClubResource
  ;

/**
 * Description
 * @constructor
 * @alias module:club.js
 * @param {TYPE} [param]
 * @param {TYPE} [?param.val=1j]
 * @example var x = new require('club.js');
 */

ClubResource = Resource.extend({
	mixin:[JSONSchema]
	,options:{
		queryset:Club.getJoin().filter({}),
		name:'club',
		labelField:'name'
	}
	,fields:{
		name:{ type:'char'}
		,description:{type:'char'}
		,active:{type:'bool',default:true}
		,members:{type:'hasmany', to: UserResource, full: false, minimal: true}
	}
})

module.exports = ClubResource
