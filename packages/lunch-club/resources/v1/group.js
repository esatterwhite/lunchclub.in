/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true*/
'use strict';
/**
 * Group.js
 * @module Group.js
 * @author 
 * @since 0.0.1
 * @requires tastypie
 * @requires Group
 * @requires moduleC
 */

var tastypie = require( 'tastypie' )
  , Resource = require('@tastypie/rethink')
  , Role = require( 'skoodge/models/role' )
  , JSONSchema = require('@tastypie/jsonschema')
  , GroupResource
  ;

/**
 * Description
 * @constructor
 * @alias module:Group.js
 * @param {TYPE} [param]
 * @param {TYPE} [?param.val=1j]
 * @example var x = new require('Group.js');
 */

GroupResource = Resource.extend({
	mixin:[JSONSchema]
	,options:{
		queryset:Role.filter({}),
		name:'role',
		pk:'name'
	}
	,fields:{
		name:{type:'char', required:true}
		,permissions:{type:'object', required:true}
	}
})

module.exports = GroupResource
