try{
	var reqlite   = require('reqlite');
	connection    = new reqlite({debug:!!process.env.REQL_DEBUG});
} catch( e ){
	console.log('reqlite connection failed');
}


var Hapi = require('hapi')
  , tastypie = require('tastypie')
  , conf = require('keef')
  , startup = require('./lib/loading/startup')
  , logger = require('bitters')
  , server
  , ui
  , api
  , starthapi
  , PORT

PORT = conf.get('PORT')
server = new Hapi.Server({});

ui = server.connection({
	host:'0.0.0.0'
  ,port:PORT
  ,labels:['ui']
});

api = server.connection({
  host:'0.0.0.0',
	port:PORT + 1,
	labels:['api']
});


// load debug UI & start up plugins
[
    require('inert')
  , require('vision')
].concat( startup.load().flat() )
    .forEach(function( plugin ){
        var attributes // plugin registration tags
          , name       // plugin name / package name
          , version    // plugin semver
          ;

        
        if( plugin && plugin.register ){
            attributes = plugin.register.attributes;
            name       = attributes.name || attributes.pkg.name;
            version    = attributes.version || attributes.pkg.version;

            logger.info('loading %s plugin ', name );
            server.register({
                register:plugin
              , options:plugin.options || ( conf.get(name.replace('-','_')) || {} ) 
            },function( err ){
                if( err ){
                    logger.error( logger.exception.getAllInfo( err ) );
                } else {
                    logger.info('plugin: %s @ %s loaded', name, version );
                }
            });
        }

        if( typeof plugin === 'function' ){
            logger.debug('executing startup script %s', plugin.name );
            plugin( server );
        }
    });

if( require.main === module ){
	server.start( function(){
    logger.info('api server running at: %s', api.info.uri);
		logger.info('ui server running at: %s', ui.info.uri);
	});
}


module.exports = server;
